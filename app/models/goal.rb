class Goal < ApplicationRecord
  validates :user, :title, :private, :completed, presence: true
  belongs_to :user
  
end
