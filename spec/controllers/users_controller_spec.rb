require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  describe 'POST #create' do
    context "with valid params" do
      it 'redirects to users index page ' do
        post :create, params: { user: { username: "notuser", password: "starwars"}}
        user = User.find_by(username: "notuser")
        expect(response).to redirect_to("/users/#{user.id}")
        expect(response).to have_http_status(302)
      end
    end

    context "with invalid params" do
      it 'renders new with status code 422' do
        post :create, params: { user: { username: "notuser"}}
        expect(response).to render_template('new')
        expect(response).to have_http_status(422)
      end
    end
  end

end
