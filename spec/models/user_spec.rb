require 'rails_helper'
require 'shoulda/matchers'

RSpec.describe User, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  describe "validations" do
    it { should validate_presence_of(:username)}
    it { should validate_presence_of(:session_token)}
    it { should validate_presence_of(:password_digest)}
    it { should validate_length_of(:password).is_at_least(6)}
  end

  subject(:user) { User.create!(username: "user", password: "starwars") }


  describe "#is_password?" do
    user = User.find_by(username: "user")
    it "returns true with valid password" do
      expect(user.is_password?("starwars")).to be true
    end

    it "returns false with invalid password" do
      expect(user.is_password?("notstarwars")).to be false
    end
  end

  describe "#reset_session_token!" do
    user = User.find_by(username: "user")
    it "reassigns user's session token" do
      old_token = user.session_token
      new_token = user.reset_session_token!
      expect(old_token).not_to eq(new_token)
    end

    it "should return the users session token" do
      expect(user.reset_session_token!).to eq(user.session_token)
    end

  end



  describe "::find_by_credentials" do
    user = User.find_by(username: "user")
    it "should return the user with correct credentials" do
      expect(User.find_by_credentials("user", "starwars")).to eq(user)
    end

    it "should return nil with incorrect credentials" do
      expect(User.find_by_credentials("user", "notstarwars")).to be nil
    end
  end

end
