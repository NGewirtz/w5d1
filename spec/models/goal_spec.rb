require 'rails_helper'
require 'shoulda/matchers'

RSpec.describe Goal, type: :model do
  describe "validations" do
    it { should validate_presence_of(:title)}
    it { should validate_presence_of(:private)}
    it { should validate_presence_of(:completed)}
    it { should validate_presence_of(:user)}
    it { should belong_to(:user)}

  end
end
