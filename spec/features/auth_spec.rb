require 'rails_helper'
require 'spec_helper'

feature 'the signup process' do

  scenario 'has a new user page' do
    visit new_user_url
    expect(page).to have_content 'New user'
  end

  feature 'signing up a user' do
    before(:each) do
      visit new_user_url
      fill_in 'username', with: 'neil'
      fill_in 'password', with: 'biscuits'
      click_on 'Create User'
    end

    scenario 'shows username on the users show page after successful signup' do
      expect(page).to have_content "neil's Profile"
    end
  end

  feature 'signing in the user' do
    before(:each) do
      visit new_session_url
      fill_in 'username', with: 'user'
      fill_in 'password', with: 'starwars'
      click_on 'Sign In'
    end
    scenario 'shows username on the users show page after successful signup' do
      expect(page).to have_content "user's Profile"
    end
  end

  feature 'signing in the uer not working' do
    before(:each) do
      visit new_session_url
      fill_in 'username', with: 'user'
      fill_in 'password', with: 'notstarwars'
      click_on 'Sign In'
    end
    scenario 'renders login page with unsuccessful login' do
      expect(page).to have_content "Create New Session"
    end
  end

  feature 'loging out the user' do
    before(:each) do
      visit new_session_url
      fill_in 'username', with: 'user'
      fill_in 'password', with: 'starwars'
      click_on 'Sign In'
      click_on 'Sign Out'
    end
    scenario 'renders login page when user logs out' do
      expect(page).to have_content "Create New Session"
    end
  end
end
